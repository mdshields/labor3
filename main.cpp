// labor3.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

void shift(int * a, int newNumber) {
    for(int i = 9; i > 0; i--){
        a[i] = a[i - 1];
    }
    a[0] = newNumber;
}


// 24 mal aufrufen mit verschiedenen x und y
void gold_code(int * a, int x, int y) {
    //int chip_s[1023];

    int oben[10] = {1,1,1,1,1,1,1,1,1,1};
    int unten[10] = {1,1,1,1,1,1,1,1,1,1};

    //2 9 oben oben

    // x y unten oben
    //1 2 5 7 8 unten unten

    // xor ist ^
    for(int i = 0; i < 1023; i++){
        int newUpper = oben[2] ^ oben[9];
        int newLower = unten[1] ^ unten[2] ^ unten[5] ^ unten[7] ^ unten[8] ^ unten[9];
        int newLowerOut = unten[x - 1] ^ unten[y - 1];

        int tmp = newLowerOut ^ oben[9];
        if (tmp == 0) {
            a[i] = -1;
        } else {
            a[i] = 1;
        }

        shift(oben, newUpper);
        shift(unten, newLower);
    }
}

void readFile(int * fileNumber, char * fileName){
    int i = 0;
    int counter = 0;
    FILE* fptr = fopen (fileName, "r");
    while (!feof (fptr) && counter < 1023) {
        fscanf (fptr, "%d", &i);
        fileNumber[counter] = i;
        counter++;
    }
    fclose (fptr);
}

void decode(int satNumber, int ids[1023], int fileNumber[1023]) {
    int sum = 0;
    int offset = 0;
    int bit = -1;
    int rL = 10.0;
    int nos = 3;

    float upperPeak = 1023.0 - (nos * (pow(-2.0, (rL + 2.0)/2) - 1));
    float lowerPeak = -1023 + (nos * (pow(2.0, (rL + 2.0)/2) - 1));

    while ((sum <= upperPeak && sum >= lowerPeak) && offset != 1023) {
        sum = 0;
        for (int i = 0; i < 1023; i++) {
            sum += ids[i] * fileNumber[(i + offset) % 1023];
        }
        offset++;
    }


    if (sum >= upperPeak || sum <= lowerPeak) {
        bit = (sum >= upperPeak) ? 1 : 0;
        cout << "Satellite " << satNumber  << " has sent Bit " << bit << " (Offset: " << offset << ")" << endl;
    }
}

int main(int argc, char * argv[]) {
    if(argv[1] == 0) {
        cout << "Please specify a test file.\n";
        exit(1);
    }

    int ids[24][1023];
    gold_code(ids[0], 2, 6);
    gold_code(ids[1] , 3, 7);
    gold_code(ids[2] , 4, 8);
    gold_code(ids[3] , 5, 9);
    gold_code(ids[4] , 1, 9);
    gold_code(ids[5] , 2, 10);
    gold_code(ids[6] , 1, 8);
    gold_code(ids[7] , 2, 9);
    gold_code(ids[8] , 3, 10);
    gold_code(ids[9] , 2, 3);
    gold_code(ids[10], 3, 4);
    gold_code(ids[11], 5, 6);
    gold_code(ids[12], 6, 7);
    gold_code(ids[13], 7, 8);
    gold_code(ids[14], 8, 9);
    gold_code(ids[15], 9, 10);
    gold_code(ids[16], 1, 4);
    gold_code(ids[17], 2, 5);
    gold_code(ids[18], 3, 6);
    gold_code(ids[19], 4, 7);
    gold_code(ids[20], 5, 8);
    gold_code(ids[21], 6, 9);
    gold_code(ids[22], 1, 3);
    gold_code(ids[23], 4, 6);

    cout << '\n';

    int n = 1;
    while(argv[n] != 0) {
        printf("Filename: %s\n", argv[n]);
        int fileNumber[1023];
        readFile(fileNumber, argv[n]);

        for (int i = 0; i < 24; i++) {
            decode(i, ids[i], fileNumber);
        }
        cout << "\n\n";
        n++;
    }

    return 0;
}
